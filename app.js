const cluster = require('cluster');
const fs = require('fs');

if (cluster.isMaster) {
	var worker = cluster.fork();
	worker.send({type:"login"});
	var count = 0;
	
	cluster.on('online', function(worker, code, signal) {
		console.log('worker start : ' + worker.process.pid);
		count = count + 1;
		worker.on('message', function (message) {
			switch(message){
				case "start" :{
					worker.kill();
					worker = cluster.fork();
					worker.send({type:"login"});
					break;
				}
				case "kill" : {
					for (const id in cluster.workers) {
						cluster.workers[id].send({type:"kill"});
					}
					process.exit(0);
					break;
				}
			}
		});
	});
	
	cluster.on('exit', function(worker, code, signal) {
		console.log('worker is dead : ' + worker.process.pid);
		count = count - 1;
		if(code != 0){
			worker = cluster.fork();
			worker.send({type:"login"});
			worker.send('Worker is dead, auto restart start');
		}
	});
	
}

if (cluster.isWorker) {
	const Discord = require('discord.js');
	const client = new Discord.Client();
	
	String.prototype.replaceAll = function(target, replacement) {
		return this.split(target).join(replacement);
	};
	
	process.on('message', function(message) {	
		switch(message.type){
			case "login" :{
				console.log("login!");
				client.login(require('./token').token);
				break;
			}
			case "kill":{
				process.exit(0);
				break;
			}
		}
	});	
	
	client.on('ready', () => {
		console.log('I am ready!');
		if(client.user.bot){
			const embed = new Discord.RichEmbed()
				.setTitle("Status")
				.setAuthor("Now Loading!!!!", client.user.avatarURL, 'https://www.youtube.com/watch?v=6kR44t-CLvI')
				.setColor(0xA99DCB)
				.setFooter("Developed by GyungDal");
			const channel = client.channels.findAll('name', 'general');
			console.log(channel);
			channel.forEach((element, index, array) => {
				console.log(element);
				element.sendMessage({embed});
			});
			
			if(client.user.id == require('./token').botId){
				console.log("My name is Hakdo bot");
				const bot = require('./bot.js');
				const running = bot(Discord, client);
			}
		}
	});
}
