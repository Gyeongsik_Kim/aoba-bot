﻿module.exports = function(Discord, client){
	var admins = [require('./token').adminId];
	
	client.on('message', message => {
		if(message.content.indexOf('h!') != 0)
			return;
		const a = message.content.indexOf(' ') != -1 ? message.content.indexOf(' ') : message.content.length;
		switch(message.content.substring(message.content.indexOf('h!') + 2, a)){
			case "pid":{
				console.log(process.pid);
				message.channel.send("\nPID : " + process.pid);
				break;
			}
		}
	});	

	client.on('message', message => {
		if(admins.indexOf(message.author.id) != -1 && message.content.includes('h!')){
			const a = message.content.indexOf(' ') != -1 ? message.content.indexOf(' ') : message.content.length;
			switch(message.content.substring(message.content.indexOf('h!') + 2, a)){
				case "kill" : {
					process.send('kill');
					break;
				}
				case "restart":{
					process.exit(-1);
					break;
				}
				case "exec":{
					console.log(message.content.replace('h!exec', '').trim());
					exec(message.content.replace('h!exec', '').trim(), (error, stdout, stderr) =>{
						if(error){
							const embed = new Discord.RichEmbed()
								.setTitle("Error")
								.setColor(0xD50000)
								.setTimestamp()
								.addField("Description", error)
								.setFooter("Hakdo bot | Developed by GyungDal", client.user.avatarURL);
							message.channel.send({embed});	
							return;
						}
						console.log(stdout);	
						const embed = new Discord.RichEmbed()
							.setTitle("Success")
							.setColor(0x76FF03)
							.setTimestamp()
							.addField("Output", stdout)
							.setFooter("Hakdo bot | Developed by GyungDal", client.user.avatarURL);
						message.channel.send({embed});	
					});
				}
			}
		}
	});
}
